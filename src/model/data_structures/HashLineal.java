package model.data_structures;

import java.util.Iterator;

public class HashLineal<Key ,Value > implements IHash<Key, Value> {

	private int N; // number of key-value pairs in the table
	private int M = 16; // size of linear-probing table
	private Key[] keys = (Key[]) new Object[M];  // the keys
	private Value[] vals  = (Value[]) new Object[M];; // the values
public int getSize()
{
return N;	
}
	public HashLineal()
	{

	}
	public HashLineal(int i) {
		// TODO Auto-generated constructor stub
		keys = (Key[]) new Object[i];
		vals = (Value[]) new Object[i];
	}
	private int hash(Key key)
	{
	return (key.hashCode() & 0x7fffffff) % M;
	}
	
	@Override
	public void put(Key key, Value val)
	{
//	if (N >= M/2)
//	resize(2*M); // double M (see text)
	int i ;
	for (i = hash(key); keys[i] != null; i = (i + 1) % M)
	if (keys[i].equals(key))
	{
	vals[i] = val;
	return;
	}
	keys[i] = key;
	vals[i] = val;
	N++;
	}

	private void resize(int cap) {
		// TODO Auto-generated method stub
		HashLineal<Key, Value> t;
		t= new HashLineal<Key, Value>(cap);
		for(int i =0; i<M; i++){
			if(keys[i] !=null){
				t.put(keys[i], vals[i]);
			}
			keys= t.keys;
			vals = t.vals;
			M= M*2;
		}
	}
	@Override
	public Value get(Key key)
	{
	for (int i = hash(key); keys[i] != null; i = (i + 1) % M)
	if (keys[i].equals(key))
	return vals[i];
	return null;
	}

	@Override
	public void delete(Key key) {
		if (!contains(key)) return;
		int i = hash(key);
		while (!key.equals(keys[i]))
		i = (i + 1) % M;
		keys[i] = null;
		vals[i] = null;
		i = (i + 1) % M;
		while (keys[i] != null) {
		Key keyToRedo = keys[i];
		Value valToRedo = vals[i];
		keys[i] = null;
		vals[i] = null;
		N--;
		put(keyToRedo, valToRedo);
		i = (i + 1) % M;
		}
		N-- ;
		if (N > 0 || N == M/8) resize(M/2);
		}
	private boolean contains(Key key) {
		// TODO Auto-generated method stub
		boolean esta= false;
		int j=0;
	
		for (int i = hash(key); i<keys.length && esta==false; i = (i + 1) % M){
			if(key.equals(keys[i]))
			{
				esta=true;
			}
		}		
		
		return esta;
	}
	@Override
	public Iterator<Key> keys() {
		// TODO Auto-generated method stub
		return null;
	}
	
	

//	//modelo normalde un metodo hash code
//	public int hashCode() {
//		int hash = 17;
//		hash = 31*hash + who.hashCode(); //tipos primitivosd e dato
//		//se usa un numero primo de preferencia peque�o (31)
//		hash = 31*hash + when.hashCode();
//		hash = 31*hash + ((Double) amount).hashCode(); //tipos no primitivos
//		return hash;
//		}
}
