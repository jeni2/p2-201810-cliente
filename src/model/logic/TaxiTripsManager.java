package model.logic;

import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import API.ITaxiTripsManager;
import model.data_structures.HashEncadenamiento;
import model.data_structures.IList;
import model.data_structures.Lista;
import model.data_structures.MyLinkedList;
import model.data_structures.MyMaxPQ;
import model.data_structures.RedBlackBST;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.FechaServicios;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;
import model.vo.ZonaServicios;
import sun.print.resources.serviceui;

public class TaxiTripsManager implements ITaxiTripsManager {
	// TODO
	// Definition of data model 
	private MyLinkedList<Servicio> services = new MyLinkedList<Servicio>();
	private MyLinkedList<Taxi> taxis = new MyLinkedList<Taxi>();
	private MyLinkedList<String> ids = new MyLinkedList<String>();
	private MyLinkedList<CompaniaServicios> compa�iaServicio = new MyLinkedList<CompaniaServicios>();
	private MyLinkedList<Compania> compa�ia = new MyLinkedList<Compania>();
	private MyLinkedList<ZonaServicios> zonaServicio = new MyLinkedList<ZonaServicios>();
	private RedBlackBST<String, MyLinkedList<String>> arbolCompa�ia = new RedBlackBST<String, MyLinkedList<String>>();
	private HashEncadenamiento<Integer, MyLinkedList<TaxiConServicios>> tablaZonas = new HashEncadenamiento<Integer, MyLinkedList<TaxiConServicios>>(78);
	
	private HashEncadenamiento<String, MyLinkedList<Servicio>> tablaTaxiServicio = new HashEncadenamiento<>(1900);
	private HashEncadenamiento<Integer, MyLinkedList<Servicio>> tablaRangos = new HashEncadenamiento<>(400);
	private HashEncadenamiento<Double, RedBlackBST<String, MyLinkedList<Servicio>>> tablaDistancias = new HashEncadenamiento<>(500);
 
	private HashEncadenamiento<Date, HashEncadenamiento<Integer, HashEncadenamiento<Integer, MyLinkedList<Servicio>>>> tablaFechaHora = new HashEncadenamiento<>(600);
	
	
	private double latitudReferencia =0.0;
    private double longitudDeReferencia =0.0;
    private int cantidadDistanciasValidas =0;

	public static final String[] DIRECCION_LARGE_JSON ={

			"./data/taxi-trips-wrvz-psew-subset-02-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-03-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-04-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-05-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-06-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-07-02-2017.json",
			"./data/taxi-trips-wrvz-psew-subset-08-02-2017.json"

	};


	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	private static final MyLinkedList<Servicio> Servicio = null;

	DateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss" );
	DateFormat formatoFecha2 = new SimpleDateFormat("yyyy-MM-dd" );
	@Override
	public void cargarSistema(String serviceFile) {
		// TODO Auto-generated method stub

		services= new  MyLinkedList <Servicio>();
		taxis= new MyLinkedList<Taxi>();
		compa�ia= new MyLinkedList<Compania>();
		ids= new MyLinkedList<String>();
		compa�iaServicio = new MyLinkedList<CompaniaServicios>();
		zonaServicio = new MyLinkedList<ZonaServicios>();

		JSONParser parser =new JSONParser();
		try
		{
			MyLinkedList<Taxi>  inscritos= new MyLinkedList<Taxi>();
			MyLinkedList<Servicio> serviciosCompa= new MyLinkedList<Servicio>();
			MyLinkedList<String> idsZonas= new MyLinkedList<String>();
			MyLinkedList<Servicio> asociadosFechaZona= new MyLinkedList<Servicio>();
			MyLinkedList<FechaServicios> fechasServicios= new MyLinkedList<FechaServicios>();

			JSONArray listaGson= (JSONArray)parser.parse(new FileReader(serviceFile));
			Iterator iter=  listaGson.iterator();
			int x= 0;
			Date fechafinal = null;
			Date fechaInicio = null;

			while(iter.hasNext())
			{
				JSONObject o=(JSONObject) listaGson.get(x);

				String company= "Independents";
				if(o.get("company") != null) {
					company= (String) o.get("company");
				}
				int community =0;
				if(o.get("dropoff_community_area") != null) {
					community= Integer.parseInt((String) o.get("dropoff_community_area"));
				}
				int dropofzone =0;
				if(o.get("dropoff_community_area") != null) {
					dropofzone= Integer.parseInt((String) o.get("dropoff_community_area"));
				}

				String tripId= " ";
				if(o.get("trip_id") != null) {
					tripId= (String) o.get("trip_id");
				}


				String taxiId= " ";
				if(o.get("taxi_id") != null) {
					taxiId= (String) o.get("taxi_id");
				}

				int tripSeconds= 0;
				if(o.get("trip_seconds") != null) {
					tripSeconds= Integer.parseInt((String) o.get("trip_seconds"));
				}




				double latitudInicio= 0;
				if(o.get("pickup_centroid_latitude") != null) {
					latitudInicio= Double.parseDouble((String) o.get("pickup_centroid_latitude"));
				}

				double longitudInicio= 0;
				if(o.get("pickup_centroid_longitude") != null) {
					longitudInicio= Double.parseDouble((String) o.get("pickup_centroid_longitude"));
				}

				if(latitudInicio!= 0.0 && longitudInicio != 0.0){
					latitudReferencia = latitudReferencia + latitudInicio;
					longitudDeReferencia = longitudDeReferencia + longitudInicio;
					cantidadDistanciasValidas++;
				}


				double tripMiles= 0;
				if(o.get("trip_miles") != null) {
					tripMiles= Double.parseDouble((String) o.get("trip_miles"));
				}
				double tripTotal= 0;
				if(o.get("trip_total") != null) {
					tripTotal= Double.parseDouble((String) o.get("trip_total"));
				}

				String StartTime ="000-00-00T00:00:00.00";
				if(o.get("trip_start_timestamp") != null)
				{
					StartTime = (String) o.get("trip_start_timestamp");

					fechaInicio= formatoFecha.parse(StartTime);
				}
				DateFormat formatoFecha2 = new SimpleDateFormat("yyyy-MM-dd" );
				String[] fechainicio2= StartTime.split("T");
				Date fecha= formatoFecha2.parse(fechainicio2[0]);
				String[] horai= fechainicio2[1].split(":");


				String EndTime ="";

				if(o.get("trip_end_timestamp") != null)
				{
					EndTime = (String) o.get("trip_end_timestamp");
					fechafinal = formatoFecha.parse(EndTime);

				}
				String[] fechafinal2= StartTime.split("T");
				String[] horaf= fechainicio2[1].split(":");

				if(o.get("trip_total") != null) {
					tripTotal= Double.parseDouble((String) o.get("trip_total"));
				}

				if(o.get("trip_total") != null) {
					tripTotal= Double.parseDouble((String) o.get("trip_total"));
				}





				Taxi taxix= new Taxi(company, taxiId);
				boolean w=false;
				for(int h=0; h<ids.size() && !w; h++){
					if(ids.getI(h).equals(taxiId))
					{
						w=true;
					}
				}
				if(w==false)
				{
					taxis.add(taxix);
					ids.add(taxix.getTaxiId());
				}

				String idZonaF= "";
				if(o.get("dropoff_census_tract") != null) {
					idZonaF= (String) o.get("dropoff_census_tract");
				}
				String idZonaI=" ";
				if(o.get("pickup_census_tract") != null) {
					idZonaI= (String) o.get("pickup_census_tract");
				}

				int PickupZona=0;
				if(o.get("pickup_community_area") != null) {
					PickupZona= Integer.parseInt((String) o.get("pickup_community_area"));
				}
				
				


				Servicio serviciox = new Servicio(taxiId, tripId, tripSeconds, tripMiles, fechaInicio, fechafinal, taxix, tripTotal, idZonaI, idZonaF, community, latitudInicio, longitudInicio, StartTime, PickupZona,dropofzone);




				MyLinkedList<String> array = new MyLinkedList<String>();
				if(arbolCompa�ia.get(company)==null){
					array.add(taxiId);
					arbolCompa�ia.put(company, array);
				}
				else{
					array= arbolCompa�ia.get(company);
					array.add(taxiId);
					arbolCompa�ia.put(company, array);
				}



				MyLinkedList<Servicio> array3 = new MyLinkedList<Servicio>();

				if(tablaTaxiServicio.get(taxiId)==null){

					array3.add(serviciox);
					tablaTaxiServicio.put(taxiId, array3);
				}
				else{
					array3= tablaTaxiServicio.get(taxiId);

					array3.add(serviciox);
					tablaTaxiServicio.put(taxiId, array3);

				}





				MyLinkedList<TaxiConServicios> array2 = new MyLinkedList<TaxiConServicios>();
				MyLinkedList<Servicio> serviciosEnZona= new MyLinkedList<>();
				if(tablaZonas.get(PickupZona)==null){

					serviciosEnZona.add(serviciox);


					TaxiConServicios taxi= new TaxiConServicios(taxiId, company,serviciosEnZona);
					array2.add(taxi);
					tablaZonas.put(PickupZona, array2);
				}
				else{
					array2= tablaZonas.get(PickupZona);
					for (int i =0; i<tablaZonas.get(PickupZona).size(); i++){
						if(tablaZonas.get(PickupZona).getI(i).getTaxiId().equals(taxiId))
						{
							serviciosEnZona=	tablaZonas.get(PickupZona).getI(i).getServicios();
						}
					}
					serviciosEnZona.add(serviciox);
					TaxiConServicios taxi= new TaxiConServicios(taxiId, company,serviciosEnZona);
					array2.add(taxi);
					tablaZonas.put(PickupZona, array2);
				}

				MyLinkedList<Servicio> s= new MyLinkedList<>();
				HashEncadenamiento<Integer, HashEncadenamiento<Integer,MyLinkedList<Servicio> >>  ha= new HashEncadenamiento<>(27);
				HashEncadenamiento<Integer, MyLinkedList<Servicio>> hb= new HashEncadenamiento<>(30);
				int hora=Integer.parseInt(horai[0]);
				int minutos=Integer.parseInt(horai[1]);
				double z=0;
				if(tablaFechaHora.get(fecha)==null){

					z= minutos/15;
					int v = (int) z;
					if(hb.get(v)!=null){
						s=hb.get(v);
					}
					s.add(serviciox);
					hb.put(v, s);

					ha.put(hora, hb);
					tablaFechaHora.put(fecha, ha);
				}
				if(tablaFechaHora.get(fecha)!=null){

					ha=tablaFechaHora.get(fecha);
//					System.out.println("fecha x "+ ha);
					if(ha.get(hora)!=null){
//						System.out.println("fecha x2 "+ ha.get(hora));
						hb= ha.get(hora);

					}

					z= minutos/15;
					int v = (int) z;
					if(hb.get(v)!=null){
			
							s=hb.get(v);

						}

						s.add(serviciox);
						hb.put(v, s);
						ha.put(hora, hb);
						tablaFechaHora.put(fecha, ha);



//						System.out.println("fecha 2 "+tablaFechaHora.get(fecha));
//						System.out.println("fecha 3 "+tablaFechaHora.get(fecha).get(hora));
//
//						System.out.println("fecha 4 "+tablaFechaHora.get(fecha).get(hora).get(v));
//						System.out.println("fecha 5  "+tablaFechaHora.get(fecha).get(hora).get(v).getI(0));
//						System.out.println("fecha 6 "+tablaFechaHora.get(fecha).get(hora).get(v).getI(0).getFechaInicio());
					}


					MyLinkedList<Servicio> servRang = new MyLinkedList<Servicio>();

					double n= tripSeconds/60;
					int a= (int) n;


					if(tablaRangos.get(a)==null){		
						servRang.add(serviciox);
						tablaRangos.put(a,servRang);
					}
					else{
						servRang= tablaRangos.get(a);

						servRang.add(serviciox);
						tablaRangos.put(a, servRang);		
					}


					services.add(serviciox);






					x++;

					iter.next();


				}

				latitudReferencia = latitudReferencia/ cantidadDistanciasValidas;
				longitudDeReferencia = longitudDeReferencia/ cantidadDistanciasValidas;

				System.out.println(cantidadDistanciasValidas);
				for(int i =0; i< services.size(); i++)
				{
					MyLinkedList<Servicio> array1= new MyLinkedList<>();
					RedBlackBST<String, MyLinkedList<Servicio>> arbol= new RedBlackBST<>();
					double key2= getDistance(latitudReferencia, longitudDeReferencia, services.getI(i).getPickupLatitud(), services.getI(i).getPickupLongitud() )*0.00062137;
					double a = key2*10;

					int e = (int) a;

					double key = (double) e/10;
					Servicio serv = services.getI(i);
					String id= serv.getTaxiId();
					if(tablaDistancias.get(key) == null){

						array1=(tablaTaxiServicio.get(serv.getTaxiId()));
						arbol.put(id, array1);
						tablaDistancias.put(key, arbol);
					}
					else {

						arbol=tablaDistancias.get(key);
						if(arbol.get(id)!=null){
							array1= arbol.get(id);
							array1.add(serv);
							arbol.put(id, array1);
						}
						else{
							array1.add(serv);
							arbol.put(id, array1);
						}


						tablaDistancias.put(key, arbol);
					}


				}

			




			System.out.println("El tama�o del arbol es : " + arbolCompa�ia.size());

			System.out.println("Elemento del arbol : " + arbolCompa�ia.get("Taxi Affiliation Services").size());

			System.out.println("tama�o de la tabla  : " + tablaZonas.get(32).size());
			System.out.println("taxi id : " + tablaZonas.get(32).getI(1).getTaxiId());
			System.out.println("cantidad de servicios segun tabla : " + tablaTaxiServicio.get(tablaZonas.get(32).getI(1).getTaxiId()).size());
			System.out.println("cantidad de servicios segun la zona : " + tablaZonas.get(32).getI(1).numeroServicios());

			System.out.println("La cantidad de servicios cargados fue: " + services.size());
			System.out.println("La cantidad de TAXIS cargados fue: " + taxis.size());
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	/**
	 * Dada una compania y una zona de inicio retornar el taxi con mas servicios (puede haber mas de uno) iniciando en la zona dada y 
	 * sus servicios ordenados en orden cronologico.
	 * @param zonaInicio: id de la zona de inicio
	 * @param compania: nombre de la compaNia
	 * @return Taxi(s) que mas servicios realizo (puede haber mas de uno) y sus respectivos servicios.
	 */
	@Override
	public MyLinkedList<TaxiConServicios> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania) {
		// TODO Auto-generated method stub

		MyLinkedList<TaxiConServicios> TaxiEnZona= tablaZonas.get(zonaInicio);
		MyLinkedList<String> TaxisCompa�ia = arbolCompa�ia.get(compania);
		MyLinkedList<TaxiConServicios> resp = new MyLinkedList<>();
		MyLinkedList<Servicio> pServicio= new MyLinkedList<>();
		TaxiConServicios resp2= new TaxiConServicios(" ", " ", pServicio);
		int b=0;
		int a=0;
		for(int i =0 ; i<TaxiEnZona.size(); i++){
			TaxiConServicios mayor= resp2 ;

			if( TaxiEnZona.getI(i).getServicios().size()> mayor.getServicios().size() && TaxisCompa�ia.contain(TaxiEnZona.getI(i).getTaxiId())	){
				mayor= TaxiEnZona.getI(i);

				a=mayor.numeroServicios();
				if(a>b)	
				{
					b=a;
				}
				System.out.println(b);
			}
		}
		for(int i=0 ; i< TaxiEnZona.size(); i++){
			if( TaxiEnZona.getI(i).getServicios().size()==b && TaxisCompa�ia.contain(TaxiEnZona.getI(i).getTaxiId())	){
				resp2= TaxiEnZona.getI(i);
				if(!resp.contain(TaxiEnZona.getI(i)))
					resp.add(resp2);
			}
		}
		System.out.println("tama�o" + resp.size());


		return  resp;
	}

	/**
	 * Dada una duracion en segundos retornar los servicios que esten en el mismo rango de duracion (mismo minuto) que la dada.
	 * Por cada servicio se debe mostrar el id del taxi, id del servicio y su duracion en segundos.
	 * @param duracion: duracion de consulta en segundos
	 * @return Lista con los servicios con duracion en el mismo minuto (rango) de la duracion dada
	 */
	@Override
	public MyLinkedList<Servicio> A2ServiciosPorDuracion(int duracion) {
		// TODO Auto-generated method stub
		double n = duracion/60;
		int b= (int) n;
		return tablaRangos.get(b);
	}


	@Override
	public IList<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}


	@Override
	public IList<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) {
		// TODO Auto-generated method stub
		return new Lista<Servicio>();
	}


	@Override
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos() {
		// TODO Auto-generated method stub
		TaxiConPuntos[] taxiPuntos= new TaxiConPuntos[taxis.size()];
		Double[] puntos= new Double[taxis.size()];

		TaxiConPuntos taxi=null;

		for(int i=0 ; i<taxis.size(); i++){
			String id =taxis.getI(i).getTaxiId();

			MyLinkedList<Servicio> servicios= tablaTaxiServicio.get(id);

			taxi= new TaxiConPuntos(taxis.getI(i).getCompany(), id, servicios);
			taxiPuntos[i] = taxi;
			puntos[i] = taxi.getPuntos();
		}


		MyMaxPQ<TaxiConPuntos,Double> p = new MyMaxPQ<>(taxiPuntos, puntos);
		p.sortHeap(taxiPuntos);

		TaxiConPuntos[] resp= new TaxiConPuntos[taxis.size()];
		for(int i =0; i< taxiPuntos.length; i++){
			resp[i]= p.delMax();
		}

		return taxiPuntos;
	}

	public double getDistance (double lat1, double lon1, double lat2, double lon2)
	{
		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth in meters
		Double latDistance = Math.toRadians(lat2-lat1);
		Double lonDistance =  Math.toRadians(lon2-lon1);
		Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos( Math.toRadians(lat1))
		* Math.cos( Math.toRadians(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;
		return distance;
	}

	@Override
	public MyLinkedList<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millasReq2C) {
		// TODO Auto-generated method stub
		MyLinkedList<Servicio> c = new MyLinkedList<>();
		double a = millasReq2C*10;
		System.out.println( "int " +a);
		int e = (int) a;
		System.out.println( "int " +e);
		double b = (double) e/10;
		
		System.out.println( "int " +b);
		
		RedBlackBST d = tablaDistancias.get(b);
        if((MyLinkedList<Servicio>) d.get(taxiIDReq2C)!=null)
       {
		 c= (MyLinkedList<Servicio>) d.get(taxiIDReq2C);
		}
		return c;
	}
	
	

	@Override
	public MyLinkedList<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora ) throws Exception  {

		MyLinkedList<Servicio> x= new MyLinkedList<>();
		MyLinkedList<Servicio> d= new MyLinkedList<>();
	
		String[] j = hora.split(":");
		
	    Date a=formatoFecha2.parse(fecha);
	    
		HashEncadenamiento<Integer, HashEncadenamiento <Integer, MyLinkedList<Servicio>>> b =tablaFechaHora.get(a);
		
		HashEncadenamiento<Integer, MyLinkedList<Servicio>> C= b.get(Integer.parseInt(j[0]));
		
		d= C.get(Integer.parseInt(j[1])/15);
		
		for(int i =0; i<d.size(); i++)
		{
			Servicio a1 = d.getI(i);
			int q=d.getI(i).getDropOffZone();
			int t= d.getI(i).getPickupZone();
		if(	q != t){
			
		x.add(a1);
			
		}
		}
		
		return x;
		
	
	}
		
		
	
	
	
	public  Date formatoFecha(String f, String h)
		{
			SimpleDateFormat formato= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

			Date fecha2 = null;
			try {

				String fh= f+ "T" + h;
				fecha2= (Date)formato.parse(fh);
			} 
			catch (ParseException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (java.text.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return fecha2;
		}
	
	
	
	
	public double[] latYlogReferencia(){
		double[] a = new double[2];
		a[0]=latitudReferencia;
		a[1] =longitudDeReferencia;
		
		return a;
	}



}
