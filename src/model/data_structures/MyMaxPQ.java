package model.data_structures;

import model.vo.TaxiConPuntos;

public class MyMaxPQ <K extends Comparable<K> , E>  {

	//--------------------------------------------------
	//Atributos
	//--------------------------------------------------
	private K[] priority;
	private E[] queue ;
	
	private int size = 0;
	
	//--------------------------------------------------
	//Constructor
	//--------------------------------------------------
	
	@SuppressWarnings("unchecked")
	public MyMaxPQ(K a[], E b[]){
		priority = a;
		 queue =b ;
	
	}
	public MyMaxPQ(int maxSize){
		priority = (K[]) new Comparable[maxSize+1];
	
	}
	
	//--------------------------------------------------
	//Metodos
	//--------------------------------------------------
	
	public boolean isEmpty(){
		return (size == 0);
	}
	
	public int size(){
		return size;
	}
	
	public void insert(K key, E element){
		priority[++size] = key;
		queue[size-1] = element;
		swim(size);
	}
	
	public K delMax(){
		K maxE = priority[1];
	
		if(size+1< priority.length && size-1 > 0)
		exch(1, size--);
		
		priority[size+1] = null;
		queue[size+1] = null;
		return maxE;
	}
	
	private boolean less(int i, int j){
		return (((Comparable) priority[i]).compareTo(priority[j]) < 0);
	}
	
	private void exch(int i, int j){
		
		K tK = priority[i];
		E tE = queue[i];
		
		
		if(i>0 && i< queue.length-1  && j< queue.length-1 && j>0 )
		{
			priority[i] = priority[j];
			queue[i] = queue[j];
		
		priority[j] = tK;
		queue[j] = tE; 
		}
	}
	
	private void swim(int k){
		while(k > 1 && less(k/2, k)){
			exch(k/2, k);
			k = k/2;
		}
	}
	
	private void sink(int k){
		while(2*k <= size){
			int j = 2*k;
			if((j < size) && less(j, j+1))
				j++;
			if(!less(k, j)) break;
				
			exch(k, j);
			k = j;
		}
	}
	public  void sortHeap(K[] a) {
		priority=a;
		int N = a.length;

		for (int k = N/2; k >= 1; k--)
			sink( k);
		while (N > 1)
		{
			exch( 1, N--);
			sink( 1 );}
	}
	
	
	// main function to do heap sort
	public void heapSort(K[] puntos)
	{
		int n = puntos.length;
	    // Build heap (rearrange array)
	    for (int i = n / 2 - 1; i >= 0; i--)
	        heapify(puntos, n, i);
	 
	    // One by one extract an element from heap
	    for (int i=n-1; i>=0; i--)
	    {
	        // Move current root to end
	        exch(0, i);
	 
	        // call max heapify on the reduced heap
	        heapify(puntos, i, 0);
	    }
	}
	 public void heapify( K[] puntos, int n, int i)
	{
	    int largest = i;  // Initialize largest as root
	    int l = 2*i + 1;  // left = 2*i + 1
	    int r = 2*i + 2;  // right = 2*i + 2
	 
	    // If left child is larger than root
	    if (l < n && ((Comparable<K>) puntos[l]).compareTo(puntos[largest])<1)
	        largest = l;
	 
	    // If right child is larger than largest so far
	    if (r < n && ((Comparable<K>)puntos[r]).compareTo(puntos[largest])>1)
	        largest = r;
	 
	    // If largest is not root
	    if (largest != i)
	    {
	        exch(i, largest);
	 
	        // Recursively heapify the affected sub-tree
	        heapify(puntos, n, largest);
	    }
	}
}

