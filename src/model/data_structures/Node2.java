package model.data_structures;

import java.util.Date;

import com.sun.javafx.scene.paint.GradientUtils.Point;

public class Node2 <Key, Value,E>{

     private Key key;
     private Value val;
	private Node2<Key, Value,E> next;
	private Node2<Key, Value,E> previous;
	
	private E item;

	public Node2 (Key pKey, Value pVal,E item) {
		setKey(pKey);
		setVal(pVal);
		next = null;
		previous=null;
		this.item = item;
	}

	

	public Node2<Key, Value, E> getNext() {
		return next;
	}

	public void setNextNode ( Node2<Key,Value, E> pNext) {
		next = pNext;
	}

	public E getItem(){
		return item;
	}

	public void setItem (E pItem) {
		item = pItem;
	}

	public boolean hasNext() {
		// TODO Auto-generated method stub
		boolean resp=false;
		if(next!=null)
		{
			resp= true;
		}
		return resp;
	}

	public Node2<Key, Value,E> getPrevious() {
		return previous;
	}

	public void setPrevious(Node2<Key,Value, E> previous) {
		this.previous = previous;
	}



	public Key getKey() {
		return key;
	}



	public void setKey(Key key) {
		this.key = key;
	}



	public Value getVal() {
		return val;
	}



	public void setVal(Value val) {
		this.val = val;
	}


}

