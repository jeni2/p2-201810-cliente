package model.data_structures;

import java.util.Iterator;




public class HashEncadenamiento<Key extends Comparable<Key>,Value  > {
	private int M ; //tama�o de la tabla
	private Node2[] cadena ; // array of cadenas


	
	private static class Node2 <Key ,Value >{

		private Key   key;
		private Value val;
		private Node2 next;

		public Node2(Object key2, Object val2, Node2 node2) {
			// TODO Auto-generated constructor stub
			this.key  = (Key) key2;
			this.val  = (Value) val2;
			this.next = node2;
		}
	}


//	public HashEncadenamiento()  {
//		this(997);
//	}
	public HashEncadenamiento  (int pM){
		this.M =pM;
		cadena =  new Node2[M];
		for (int i=0; i<M; i++){
			cadena[i]= new Node2(null, null, null);
		}
	}

	private int hash(Key key) 
	{
		return ((key.hashCode() & 0x7fffffff) % M); 
	}

	public void put(Key key, Value val)
	{		

		if (cadena.length/M> 6.0)
		{resize();} // double M (see text)	

		int i =  hash(key);
		for (Node2 x = cadena[i]; x != null; x = x.next)
		{
			if (key.equals(x.key))
			{

				x.val = val;
				return;
			}
		}
		cadena[i] = new Node2(key, val, cadena[i]);

	}

	public void resize() {
		// TODO Auto-generated method stub
		HashEncadenamiento<Key,Value> t;
		M= 2*M;
		t= new HashEncadenamiento<Key, Value>(M);
	
		for(int i =0; i<M; i++){
			if(cadena[i] !=null){
				Key k= (Key) cadena[i].key;
				Value v = (Value) cadena[i].val;
				t.put(k,v);
			}
			M= t.M;
		}
	}
	public void reHash(){
		M = M*2;
		for(int i=0 ; i<cadena.length; i++){

			for (Node2 x = cadena[i]; x != null; x = x.next)
			{
				Value v = (Value) x.val;
				Key k= (Key) x.key;
				x=null;
				put(k,v);
			}
		}
		System.out.println(M);}

	public Value get(Key k) {
		// TODO Auto-generated method stub	
		int i = hash(k);

		for (Node2 x = cadena[i]; x != null; x = x.next){


			if (k.equals(x.key)){

				return (Value) x.val;
			}
		}
		return null;

	}



	/**
	 *duevelve todo slos valores en una posicion de la cadena 
	 */
	public Iterable<Key> keys(Key k)  {
		int hash= k.hashCode();
		MyQueue<Value> valores = new MyQueue<Value>();

		for (Node2 x = cadena[hash]; x != null; x = x.next)
		{
			valores.enqueue((Value) x.val);
		}
		return (Iterable<Key>) valores;
	}
	//modelo normalde un metodo hash code

	public int getM (){
		return M;
	}

}