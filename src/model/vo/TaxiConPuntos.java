package model.vo;

import model.data_structures.MyLinkedList;

public class TaxiConPuntos  implements Comparable<TaxiConPuntos> {
 
	private String compa�ia ;
	private double puntos= 0.0;
	private String taxiId;
	private MyLinkedList<Servicio> servicios;

	public TaxiConPuntos(String c, String id,MyLinkedList<Servicio> pServicios) {
		compa�ia= c;
		taxiId =id;
		servicios= pServicios;
		// TODO Auto-generated constructor stub
	}
	
	/**
     * @return puntos - puntos de un Taxi
     */
//	Para calcular los puntos asignados a un taxi, se toma cada uno de los servicios
//	prestados por dicho taxi, se suma el total de millas recorridas y el total de dinero
//	recibido por dicho taxi en todos sus servicios y se divide el total de dinero recibido
//	entre el total de millas recorridas, multiplicado por el total de servicios prestados (se
//	toman en cuenta los servicios para los cuales se tiene una distancia mayor a 0.0 y se
//	paga un valor mayor a U$ 0.0).
	public double getPuntos(){
	double millas =0;
	double dinero=0;
	int  contadorServicios =0;
	
		for (int i = 0; i<servicios.size(); i++){
			if(servicios.getI(i).getTripMiles()>0.0 && servicios.getI(i).getTrp_total()>0.0)
			{	
			contadorServicios ++;
			millas +=servicios.getI(i).getTripMiles();
			dinero += servicios.getI(i).getTrp_total();
			}
		}
		
		puntos = (dinero/millas)* contadorServicios;
		
		return puntos;
	}

	@Override
	public int compareTo(TaxiConPuntos o) {
		// TODO Auto-generated method stub
		return taxiId.compareTo(o.getTaxiId());
	}

	public String getCompa�ia() {
		return compa�ia;
	}

	public void setCompa�ia(String compa�ia) {
		this.compa�ia = compa�ia;
	}

	public String getTaxiId() {
		return taxiId;
	}

	public void setTaxiId(String taxiId) {
		this.taxiId = taxiId;
	}
}
