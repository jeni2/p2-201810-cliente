package model.vo;

import model.data_structures.IList;
import model.data_structures.MyLinkedList;

public class TaxiConServicios implements Comparable<TaxiConServicios>{

    private String taxiId;
    private String compania;
    private MyLinkedList<Servicio> servicios;

    public TaxiConServicios(String taxiId, String compania, MyLinkedList<Servicio> pServicios){
        this.taxiId = taxiId;
        this.compania = compania;
        this.servicios= pServicios;
        // this.servicios = new List<Service>(); // inicializar la lista de servicios 
    }

    public String getTaxiId() {
        return taxiId;
    }

    public String getCompania() {
        return compania;
    }

    public MyLinkedList<Servicio> getServicios()
    {
    	return servicios;
    }
    
    public int numeroServicios(){
        return servicios.size();
    }

    public void agregarServicio(Servicio servicio){
        servicios.add(servicio);
    }

    @Override
    public int compareTo(TaxiConServicios o) {
        return taxiId.compareTo( o.getTaxiId());
    }

    public void print(){
        System.out.println(Integer.toString(numeroServicios())+" servicios "+" Taxi: "+taxiId);
        for(int i =0; i< servicios.size(); i++){
            System.out.println("\t"+servicios.getI(i).getStartTime());
        }
        System.out.println("___________________________________");;
    }
}
